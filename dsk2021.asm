.model SMALL
.code
	ORG 100h
data :	jmp proses
	lnim		db 13,10,'NIM : $'
	lpassword	db 13,10,'Password : $'
	lbenar		db 13,10,'BENAR $'
	lsalah		db 13,10,'SALAH $'

	vnim		db 23,?,23 dup(?)
	vpassword	db 23,?,23 dup(?)

proses :
	mov ah,09h
	lea dx,lnim
	int 21h

	mov ah,0ah
	lea dx,vnim
	int 21h

	mov ah,09h
	lea dx,lpassword
	int 21h

	mov ah,0ah
	lea dx,vpassword
	int 21h

	lea si,vnim
	lea di,vpassword

	cld
	mov cx,23
	rep cmpsb
	jne gagal

	mov ah,09h
	lea dx,lsalah
	int 21h
	jmp proses

gagal :
	mov ah,09h
	lea dx,lbenar
	int 21h
	jmp exit

exit :
	int 20h
end data
